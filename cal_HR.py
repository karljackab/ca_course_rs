import torch
import torch.nn as nn
import json
import models.dataloader as loader
from collections import defaultdict
from tqdm import tqdm
from torch.utils.data import DataLoader

import models.course as course
import models.student as student

##############
Cos_name_weight_pth = 'weights/Dot/Cos_name_60_0.69345.pkl'
Cos_embed_weight_pth = 'weights/Dot/Cos_embed_60_0.69345.pkl'
Std_embed_weight_pth = 'weights/Dot/Std_embed_60_0.69345.pkl'
##############
###################
batch_size = 64
Top_K = 10
###################

device = torch.device('cuda')

eval_dataset = loader.CA_eval_dataset(data_type='test_HR')

def loader_collatfn(data):
    sid = []
    record, cos_idx, record_order, sgrade, cos_pos_idx, cos_pref = [], [], [], [], [], []
    for item in data:
        sid.append(item[0])
        record.append(item[1])
        cos_idx.append(item[3])
        record_order.append(torch.tensor(item[2]).to(device))
        sgrade.append(item[4])
        cos_pos_idx.append(item[5].unsqueeze(1).to(device))
        cos_pref.append(item[6])
    record = torch.stack(record, dim=0)
    cos_idx = torch.stack(cos_idx, dim=0).view(-1, 1)
    sgrade = torch.tensor(sgrade).float().view(-1, 1)
    return sid, record.to(device), record_order, cos_idx.to(device),\
         sgrade.to(device), cos_pos_idx, cos_pref

train_data = DataLoader(
    dataset = eval_dataset,
    batch_size = batch_size,
    collate_fn = loader_collatfn
)

Cos_name_model = torch.load(Cos_name_weight_pth).to(device)
Course_embedding = torch.load(Cos_embed_weight_pth).to(device)
Student_embedding = torch.load(Std_embed_weight_pth).to(device)


min_loss = 100
max_f = 0

print('Start Evaluation')
Cos_name_model.eval()
Course_embedding.eval()
Student_embedding.eval()

with open('data/word2idx.json', 'rb') as f:
    Cos_name_model.word2idx = json.load(f)
with open('data/idx2word.json', 'rb') as f:
    Cos_name_model.idx2word = json.load(f)
with open('data/num_to_cos.json', 'r') as f:
    Cos_name_model.idx2cos = json.load(f)
    num_to_cos = Cos_name_model.idx2cos

score_res = defaultdict(list)
std_pos_list = defaultdict(list)
with torch.no_grad():
    for sid, record, record_order, cos_idx, sgrade, cos_pos_idx, cos_pref in tqdm(train_data):
        s_embed = Student_embedding(record, record_order, Cos_name_model, sgrade, cos_pos_idx)
        c_embed = Course_embedding(cos_idx, Cos_name_model)

        s_embed, c_embed = s_embed/torch.norm(s_embed, dim=1).unsqueeze(1), c_embed/torch.norm(c_embed, dim=1).unsqueeze(1)
        pred = torch.bmm(s_embed.unsqueeze(1), c_embed.unsqueeze(2)).view(-1)
        pred = torch.sigmoid(pred)

        for student, cos, score, pref in zip(sid, cos_idx.view(-1).tolist(), pred.view(-1), cos_pref):
            score_res[student].append((cos, float(score)))
            if pref == 1:
                std_pos_list[student].append(cos)

    cnt, tot = 0, 0 
    for student in score_res:
        score_res[student].sort(key=lambda x:x[1], reverse=True)
        rs_cos_list = []
        for cos_id, _ in score_res[student][:Top_K]:
            rs_cos_list.append(cos_id)
        
        ## calculate Hit Ratio
        for gold in std_pos_list[student]:
            tot += 1
            if gold in rs_cos_list:
                cnt += 1
    print(f"Hit Ratio: {cnt/tot}")