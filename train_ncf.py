import os
import torch
import torch.nn as nn
from torch.utils.data import DataLoader

from NCF import model, ca_loader

##############
ver = 'NCF_enrolled_ver'
##############
###################
Epoch = 200
batch_size = 64
LR = 0.001
weight_lambda = 0.01
###################
if not os.path.isdir(f'log/{ver}'):
    os.mkdir(f'log/{ver}')
if not os.path.isdir(f'weights/{ver}'):
    os.mkdir(f'weights/{ver}')
device = torch.device('cuda')
train_dataset = ca_loader.CA_NCF_dataset('train')
test_dataset = ca_loader.CA_NCF_dataset('test')

train_data = DataLoader(
    dataset = train_dataset,
    batch_size = batch_size,
    shuffle = True
)
test_data = DataLoader(
    dataset = test_dataset,
    batch_size = batch_size
)

NCF_model = model.NCF(user_num=1628, item_num=311, factor_num=32, num_layers=5, \
    dropout=0.1, model='NeuMF-end').to(device)

optimizer = torch.optim.Adam(NCF_model.parameters(),\
     lr=LR, weight_decay = weight_lambda)

Loss = nn.BCELoss()

min_loss = 100
max_f = 0
for epoch in range(Epoch):
    print(f"Epoch {epoch} Training")
    tot_loss = 0
    TP, FP, FN = 0, 0, 0
    TP_sum, FP_sum, FN_sum = 0, 0, 0
    tot_sum = 0
    NCF_model.train()
    for itera, (std_idx, cos_idx, label) in enumerate(train_data):
        #####################################
        std_idx, cos_idx, label = std_idx.to(device), cos_idx.to(device),\
             label.float().to(device)
        pred = NCF_model(std_idx, cos_idx)
        pred = torch.sigmoid(pred)
        loss = Loss(pred, label)
        ############################################
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        with torch.no_grad():
            for pred, gold in zip((pred>=0.5).int(), label.int()):
                if gold == 1 and pred == 1:
                    TP += 1
                elif gold == 0 and pred == 1:
                    FP += 1
                elif gold == 1 and pred == 0:
                    FN += 1

            TP_sum += TP
            FP_sum += FP
            FN_sum += FN
            TP, FP, FN = 0, 0, 0
            tot_loss += loss
            tot_sum += len(label)

            if itera % 100 == 0:
                if TP_sum == 0 and FP_sum == 0:
                    prec = 0
                else:
                    prec = TP_sum/(TP_sum+FP_sum)
                if TP_sum == 0 and FN_sum == 0:
                    recall = 0
                else:
                    recall = TP_sum/(TP_sum+FN_sum)
                if TP_sum == 0 and FP_sum == 0 and FN_sum == 0:
                    f_score = 0
                else:
                    f_score = 2*TP_sum / (2*TP_sum+FP_sum+FN_sum)
                print(f"Iter {itera}: Loss: {tot_loss/(itera+1):.8f}, Prec {prec:.5f}, recall {recall:.5f}, F-Score {f_score:.5f}")
    
    print(TP_sum, FP_sum, FN_sum)
    prec = TP_sum/(TP_sum+FP_sum)
    recall = TP_sum/(TP_sum+FN_sum)
    f_score = 2*TP_sum / (2*TP_sum+FP_sum+FN_sum)
    tot_loss = tot_loss/(itera+1)
    print(f"Epoch {epoch}: Loss {tot_loss:.8f}, Prec {prec:.5f}, recall {recall:.5f}, F-Score {f_score:.5f}")

    with open(f'log/{ver}/train_log', 'a') as f:
        f.write(f"Epoch {epoch}: Loss {tot_loss:.8f}, Prec {prec:.5f}, recall {recall:.5f}, F-Score {f_score:.5f}\n")

    #######################
    print(f"Epoch {epoch} Testing")
    tot_loss = 0
    TP, FP, FN = 0, 0, 0
    TP_sum, FP_sum, FN_sum = 0, 0, 0
    tot_sum = 0
    NCF_model.eval()
    with torch.no_grad():
        for itera, (std_idx, cos_idx, label) in enumerate(test_data):
            std_idx, cos_idx, label = std_idx.to(device), cos_idx.to(device),\
                label.float().to(device)
            #####################################
            pred = NCF_model(std_idx, cos_idx)
            pred = torch.sigmoid(pred)
            loss = Loss(pred, label)
            ############################################
            
            for pred, gold in zip((pred>=0.5).int(), label.int()):
                if gold == 1 and pred == 1:
                    TP += 1
                elif gold == 0 and pred == 1:
                    FP += 1
                elif gold == 1 and pred == 0:
                    FN += 1

            TP_sum += TP
            FP_sum += FP
            FN_sum += FN
            TP, FP, FN = 0, 0, 0
            tot_loss += loss
            tot_sum += len(label)

    print(TP_sum, FP_sum, FN_sum)
    prec = TP_sum/(TP_sum+FP_sum)
    recall = TP_sum/(TP_sum+FN_sum)
    f_score = 2*TP_sum / (2*TP_sum+FP_sum+FN_sum)
    tot_loss = tot_loss/(itera+1)
    print(f"Epoch {epoch}: Loss {tot_loss:.8f}, Prec {prec:.5f}, recall {recall:.5f}, F-Score {f_score:.5f}")

    with open(f'log/{ver}/test_log', 'a') as f:
        f.write(f"Epoch {epoch}: Loss {tot_loss:.8f}, Prec {prec:.5f}, recall {recall:.5f}, F-Score {f_score:.5f}\n")

    if tot_loss < min_loss:
        print("Save Model")
        if f_score > max_f:
            max_f = f_score
        min_loss = tot_loss
        torch.save(NCF_model, f'weights/{ver}/NCF_{epoch}_{f_score:.5f}.pkl')
    elif f_score > max_f:
        print("Save Model")
        max_f = f_score
        torch.save(NCF_model, f'weights/{ver}/NCF_{epoch}_{f_score:.5f}.pkl')