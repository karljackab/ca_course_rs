import os
import random
import csv
import torch
import torch.nn as nn
import models.dataloader as loader
from torch.utils.data import DataLoader
from collections import defaultdict

import models.course as course
import models.student as student

##############
ver = 'Dot_30pos'
##############
prob = True
mask = True     ## Set some of positive course as negative (for generalization)
add_l1_loss = True
###################
Epoch = 200
batch_size = 64
LR = 0.001
weight_lambda = 0.001
show_iter = 10
# dup_threshold = 15
# dup_weight = 0.01
###################

if not os.path.isdir(f'log/{ver}'):
    os.mkdir(f'log/{ver}')
if not os.path.isdir(f'weights/{ver}'):
    os.mkdir(f'weights/{ver}')

device = torch.device('cuda')

train_dataset = loader.CA_dataset('train', prob=prob)
test_dataset = loader.CA_dataset('test', prob=prob)

def loader_collatfn(data):
    sid, record, cos_idx, cos_pref, record_order, sgrade, cos_pos_idx = [], [], [], [], [], [], []
    for item in data:
        sid.append(item[0])
        record.append(item[1])
        cos_idx.append(item[3])
        if mask and item[4] >= 0.5 and random.random() < 0.4:
            cos_pref.append(torch.tensor(0.))
        else:
            cos_pref.append(item[4])
        record_order.append(torch.tensor(item[2]).to(device))
        sgrade.append(item[5])
        cos_pos_idx.append(item[6].unsqueeze(1).to(device))
    record = torch.stack(record, dim=0)
    cos_idx = torch.stack(cos_idx, dim=0).view(-1, 1)
    cos_pref = torch.stack(cos_pref, dim=0)
    sgrade = torch.tensor(sgrade).float().view(-1, 1)
    return sid, record.to(device), record_order, cos_idx.to(device),\
         cos_pref.to(device), sgrade.to(device), cos_pos_idx

train_data = DataLoader(
    dataset = train_dataset,
    batch_size = batch_size,
    shuffle = True,
    collate_fn=loader_collatfn
)
test_data = DataLoader(
    dataset = test_dataset,
    batch_size = batch_size,
    collate_fn=loader_collatfn
)

Cos_name_model = course.Cos_embed(device).to(device)
Course_embedding = course.Course_embedding_whole().to(device)
Student_embedding = student.Student_embedding_whole().to(device)

optimizer = torch.optim.Adam([
    {'params': Cos_name_model.parameters()},
    {'params': Course_embedding.parameters()},
    {'params': Student_embedding.parameters()},
], lr=LR, weight_decay = weight_lambda)


# Loss = nn.BCELoss()
def handmade_BCELoss(x, y):
    n = len(x)
    loss = (-(30*y*torch.log(x)+(1-y)*torch.log(1-x)).sum()/n)
    return loss

def WLoss(mu1, sigma1, mu2, sigma2):
    return torch.norm(mu1-mu2, dim=1)**2 + sigma1.sum(dim=1) + sigma2.sum(dim=1) - 2*torch.sqrt(sigma1*sigma2).sum(dim=1)

min_loss = 100
max_f = 0
tot_train_len = len(train_data)
tot_test_len = len(test_data)
for epoch in range(Epoch):
    print(f"Epoch {epoch} Training")
    tot_loss = 0
    TP, FP, FN = 0, 0, 0
    TP_sum, FP_sum, FN_sum = 0, 0, 0
    tot_sum = 0
    Cos_name_model.train()
    Course_embedding.train()
    Student_embedding.train()
    dup_loss = torch.zeros(311).to(device)
    score_res = defaultdict(list)
    std_pos_list = defaultdict(list)
    for itera, (sid, record, record_order, cos_idx, cos_pref, sgrade, cos_pos_idx) in enumerate(train_data):
        #####################################
        if ver[:8] == 'Dup_Loss':
            s_embed = Student_embedding(record, record_order, Cos_name_model, sgrade, cos_pos_idx)
            c_embed = Course_embedding(cos_idx, Cos_name_model)
            s_embed, c_embed = s_embed/torch.norm(s_embed, dim=1).unsqueeze(1), c_embed/torch.norm(c_embed, dim=1).unsqueeze(1)
            pred = torch.bmm(s_embed.unsqueeze(1), c_embed.unsqueeze(2)).view(-1)
            pred = torch.sigmoid(pred)
            dup_loss[cos_idx.view(-1)] += pred.view(-1)
            loss = Loss(pred, cos_pref)
            
            if itera % show_iter == 0:
                print(dup_loss)
                dup_loss = torch.max(dup_loss-dup_threshold, torch.zeros(dup_loss.shape).to(device)).sum()
                print(dup_loss)
                loss += dup_loss*dup_weight
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                dup_loss = torch.zeros(311).to(device)
            else:
                optimizer.zero_grad()
                loss.backward(retain_graph=True)
                optimizer.step()
        else:
            s_embed = Student_embedding(record, record_order, Cos_name_model, sgrade, cos_pos_idx)
            c_embed = Course_embedding(cos_idx, Cos_name_model)
            s_embed, c_embed = s_embed/torch.norm(s_embed, dim=1).unsqueeze(1), c_embed/torch.norm(c_embed, dim=1).unsqueeze(1)
            pred = torch.bmm(s_embed.unsqueeze(1), c_embed.unsqueeze(2)).view(-1)
            pred = torch.sigmoid(pred)
            # loss = Loss(pred, cos_pref)
            loss = handmade_BCELoss(pred, cos_pref)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
        ############################################
        with torch.no_grad():
            for student, cos, score, pref in zip(sid, cos_idx.view(-1).tolist(), pred.view(-1), cos_pref):
                score_res[student].append((cos, float(score)))
                if pref >= 0.5:
                    std_pos_list[student].append(cos)

            ## Store predict and ground truth result
            for predict, gold in zip(pred, cos_pref):
                if gold >= 0.5 and predict >= 0.5:
                    TP += 1
                elif gold < 0.5 and predict >= 0.5:
                    FP += 1
                elif gold >= 0.5 and predict < 0.5:
                    FN += 1

            TP_sum += TP
            FP_sum += FP
            FN_sum += FN
            TP, FP, FN = 0, 0, 0
            tot_loss += loss
            tot_sum += len(cos_pref)

            if itera % show_iter == 0:
                if TP_sum == 0 and FP_sum == 0:
                    prec = 0
                else:
                    prec = TP_sum/(TP_sum+FP_sum)
                if TP_sum == 0 and FN_sum == 0:
                    recall = 0
                else:
                    recall = TP_sum/(TP_sum+FN_sum)
                if TP_sum == 0 and FP_sum == 0 and FN_sum == 0:
                    f_score = 0
                else:
                    f_score = 2*TP_sum / (2*TP_sum+FP_sum+FN_sum)
                print(f"Iter {itera}/{tot_train_len}: Loss: {tot_loss/(itera+1):.8f}, Prec {prec:.5f}, recall {recall:.5f}, F-Score {f_score:.5f}")

    print('================================')
    for key in ['0516003','0516004','0516005','0516007']:
        score_res[key].sort(key=lambda x:x[1], reverse=True)
        std_pos_list[key].sort(reverse=True)
        print(f'{key}')
        print(f'pred: {score_res[key][:5]}')
        print(f'gold: {std_pos_list[key][:5]}')
    print('================================')
    
    if TP_sum == 0 and FP_sum == 0:
        prec = 0
    else:
        prec = TP_sum/(TP_sum+FP_sum)
    if TP_sum == 0 and FN_sum == 0:
        recall = 0
    else:
        recall = TP_sum/(TP_sum+FN_sum)
    if TP_sum == 0 and FP_sum == 0 and FN_sum == 0:
        f_score = 0
    else:
        f_score = 2*TP_sum / (2*TP_sum+FP_sum+FN_sum)
    tot_loss = tot_loss/(itera+1)
    print(f"Epoch {epoch}: Loss {tot_loss:.8f}, Prec {prec:.5f}, recall {recall:.5f}, F-Score {f_score:.5f}")

    with open(f'log/{ver}/train_log', 'a') as f:
        f.write(f"Epoch {epoch}: Loss {tot_loss:.8f}, Prec {prec:.5f}, recall {recall:.5f}, F-Score {f_score:.5f}\n")

    #######################
    print(f"Epoch {epoch} Testing")
    tot_loss = 0
    TP, FP, FN = 0, 0, 0
    TP_sum, FP_sum, FN_sum = 0, 0, 0
    tot_sum = 0
    Cos_name_model.eval()
    Course_embedding.eval()
    Student_embedding.eval()
    score_res = defaultdict(list)
    std_pos_list = defaultdict(list)
    with torch.no_grad():
        for itera, (sid, record, record_order, cos_idx, cos_pref, sgrade, cos_pos_idx) in enumerate(test_data):
            if ver[:8] == 'Dup_Loss':
                s_embed = Student_embedding(record, record_order, Cos_name_model, sgrade, cos_pos_idx)
                c_embed = Course_embedding(cos_idx, Cos_name_model)
                s_embed, c_embed = s_embed/torch.norm(s_embed, dim=1).unsqueeze(1), c_embed/torch.norm(c_embed, dim=1).unsqueeze(1)
                pred = torch.bmm(s_embed.unsqueeze(1), c_embed.unsqueeze(2)).view(-1)
                pred = torch.sigmoid(pred)
                dup_loss[cos_idx.view(-1)] += pred.view(-1)
                loss = Loss(pred, cos_pref)
                if itera % show_iter == 0:
                    dup_loss = torch.max(dup_loss-dup_threshold, torch.zeros(dup_loss.shape).to(device)).sum()
                    loss += dup_loss*dup_weight
                    dup_loss = torch.zeros(311).to(device)
            else:
                s_embed = Student_embedding(record, record_order, Cos_name_model, sgrade, cos_pos_idx)
                c_embed = Course_embedding(cos_idx, Cos_name_model)
                s_embed, c_embed = s_embed/torch.norm(s_embed, dim=1).unsqueeze(1), c_embed/torch.norm(c_embed, dim=1).unsqueeze(1)
                pred = torch.bmm(s_embed.unsqueeze(1), c_embed.unsqueeze(2)).view(-1)
                pred = torch.sigmoid(pred)
                # loss = Loss(pred, cos_pref)
                loss = handmade_BCELoss(pred, cos_pref)
            
            for student, cos, score, pref in zip(sid, cos_idx.view(-1).tolist(), pred.view(-1), cos_pref):
                score_res[student].append((cos, float(score)))
                if pref >= 0.5:
                    std_pos_list[student].append(cos)

            for predict, gold in zip(pred, cos_pref):
                if gold >= 0.5 and predict >= 0.5:
                    TP += 1
                elif gold < 0.5 and predict >= 0.5:
                    FP += 1
                elif gold >= 0.5 and predict < 0.5:
                    FN += 1

            TP_sum += TP
            FP_sum += FP
            FN_sum += FN
            TP, FP, FN = 0, 0, 0
            tot_loss += loss
            tot_sum += len(cos_pref)

            if itera % show_iter == 0:
                if TP_sum == 0 and FP_sum == 0:
                    prec = 0
                else:
                    prec = TP_sum/(TP_sum+FP_sum)
                if TP_sum == 0 and FN_sum == 0:
                    recall = 0
                else:
                    recall = TP_sum/(TP_sum+FN_sum)
                if TP_sum == 0 and FP_sum == 0 and FN_sum == 0:
                    f_score = 0
                else:
                    f_score = 2*TP_sum / (2*TP_sum+FP_sum+FN_sum)
                print(f"Iter {itera}/{tot_test_len}: Loss: {tot_loss/(itera+1):.8f}, Prec {prec:.5f}, recall {recall:.5f}, F-Score {f_score:.5f}")

        with open(f'test_{ver}_gold.csv', 'w') as f:
            for key in std_pos_list:
                writer = csv.writer(f)
                writer.writerow([key] + std_pos_list[key])
        with open(f'test_{ver}_pred.csv', 'w') as f:
            for key in score_res:
                writer = csv.writer(f)
                score_res[key].sort(key=lambda x:x[1], reverse=True)
                data = [key]
                for record in score_res[key]:
                    if record[1] < 0.5:
                        break
                    data.append(record[0])
                writer.writerow(data)

    if TP_sum == 0 and FP_sum == 0:
        prec = 0
    else:
        prec = TP_sum/(TP_sum+FP_sum)
    if TP_sum == 0 and FN_sum == 0:
        recall = 0
    else:
        recall = TP_sum/(TP_sum+FN_sum)
    if TP_sum == 0 and FP_sum == 0 and FN_sum == 0:
        f_score = 0
    else:
        f_score = 2*TP_sum / (2*TP_sum+FP_sum+FN_sum)
    tot_loss = tot_loss/(itera+1)
    print(f"Epoch {epoch}: Loss {tot_loss:.8f}, Prec {prec:.5f}, recall {recall:.5f}, F-Score {f_score:.5f}")

    with open(f'log/{ver}/test_log', 'a') as f:
        f.write(f"Epoch {epoch}: Loss {tot_loss:.8f}, Prec {prec:.5f}, recall {recall:.5f}, F-Score {f_score:.5f}\n")

    if tot_loss < min_loss:
        print("Save Model")
        if f_score > max_f:
            max_f = f_score
        min_loss = tot_loss
        torch.save(Cos_name_model, f'weights/{ver}/Cos_name_{epoch}_{f_score:.5f}.pkl')
        torch.save(Course_embedding, f'weights/{ver}/Cos_embed_{epoch}_{f_score:.5f}.pkl')
        torch.save(Student_embedding, f'weights/{ver}/Std_embed_{epoch}_{f_score:.5f}.pkl')
    elif f_score > max_f:
        print("Save Model")
        max_f = f_score
        torch.save(Cos_name_model, f'weights/{ver}/Cos_name_{epoch}_{f_score:.5f}.pkl')
        torch.save(Course_embedding, f'weights/{ver}/Cos_embed_{epoch}_{f_score:.5f}.pkl')
        torch.save(Student_embedding, f'weights/{ver}/Std_embed_{epoch}_{f_score:.5f}.pkl')