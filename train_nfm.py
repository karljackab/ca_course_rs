import os
import torch
import torch.nn as nn
from torch.utils.data import DataLoader

from NFM import model, ca_loader

##############
ver = 'NFM_more'
##############
###################
Epoch = 200
batch_size = 64
LR = 0.001
weight_lambda = 0.01
###################
if not os.path.isdir(f'log/{ver}'):
    os.mkdir(f'log/{ver}')
if not os.path.isdir(f'weights/{ver}'):
    os.mkdir(f'weights/{ver}')
device = torch.device('cuda')
train_dataset = ca_loader.CA_NFM_dataset('train')
test_dataset = ca_loader.CA_NFM_dataset('test')

train_data = DataLoader(
    dataset = train_dataset,
    batch_size = batch_size,
    shuffle = True
)
test_data = DataLoader(
    dataset = test_dataset,
    batch_size = batch_size
)

NFM_model = model.NFM(num_features=309, num_factors=64, act_function='relu', \
    layers=[64, 64, 64], batch_norm=True, drop_prob=[0.5, 0.3], pretrain_FM=None).to(device)

optimizer = torch.optim.Adam(NFM_model.parameters(),\
     lr=LR)

Loss = nn.BCELoss(reduction='sum')

min_loss = 100
max_f = 0
for epoch in range(Epoch):
    print(f"Epoch {epoch} Training")
    tot_loss = 0
    TP, FP, FN = 0, 0, 0
    TP_sum, FP_sum, FN_sum = 0, 0, 0
    tot_sum = 0
    NFM_model.train()
    for itera, (std_idx, cos_idx, cos_val, label) in enumerate(train_data):
        #####################################
        cos_idx, cos_val, label = cos_idx.to(device), cos_val.to(device),\
             label.float().to(device)
        pred = NFM_model(cos_idx, cos_val)
        pred = torch.sigmoid(pred)
        loss = Loss(pred, label)
        ############################################
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        with torch.no_grad():
            for pred, gold in zip((pred>=0.5).int(), label.int()):
                if gold == 1 and pred == 1:
                    TP += 1
                elif gold == 0 and pred == 1:
                    FP += 1
                elif gold == 1 and pred == 0:
                    FN += 1

            TP_sum += TP
            FP_sum += FP
            FN_sum += FN
            TP, FP, FN = 0, 0, 0
            tot_loss += loss
            tot_sum += len(label)

            if itera % 100 == 0:
                if TP_sum == 0 and FP_sum == 0:
                    prec = 0
                else:
                    prec = TP_sum/(TP_sum+FP_sum)
                if TP_sum == 0 and FN_sum == 0:
                    recall = 0
                else:
                    recall = TP_sum/(TP_sum+FN_sum)
                if TP_sum == 0 and FP_sum == 0 and FN_sum == 0:
                    f_score = 0
                else:
                    f_score = 2*TP_sum / (2*TP_sum+FP_sum+FN_sum)
                print(f"Iter {itera}: Loss: {tot_loss/(itera+1):.8f}, Prec {prec:.5f}, recall {recall:.5f}, F-Score {f_score:.5f}")
    
    print(TP_sum, FP_sum, FN_sum)
    if TP_sum == 0 and FP_sum == 0:
        prec = 0
    else:
        prec = TP_sum/(TP_sum+FP_sum)
    if TP_sum == 0 and FN_sum == 0:
        recall = 0
    else:
        recall = TP_sum/(TP_sum+FN_sum)
    if TP_sum == 0 and FP_sum == 0 and FN_sum == 0:
        f_score = 0
    else:
        f_score = 2*TP_sum / (2*TP_sum+FP_sum+FN_sum)
    tot_loss = tot_loss/(itera+1)
    print(f"Epoch {epoch}: Loss {tot_loss:.8f}, Prec {prec:.5f}, recall {recall:.5f}, F-Score {f_score:.5f}")

    with open(f'log/{ver}/train_log', 'a') as f:
        f.write(f"Epoch {epoch}: Loss {tot_loss:.8f}, Prec {prec:.5f}, recall {recall:.5f}, F-Score {f_score:.5f}\n")

    #######################
    print(f"Epoch {epoch} Testing")
    tot_loss = 0
    TP, FP, FN = 0, 0, 0
    TP_sum, FP_sum, FN_sum = 0, 0, 0
    tot_sum = 0
    NFM_model.eval()
    with torch.no_grad():
        for itera, (std_idx, cos_idx, cos_val, label) in enumerate(test_data):
            cos_idx, cos_val, label = cos_idx.to(device), cos_val.to(device),\
                label.float().to(device)
            pred = NFM_model(cos_idx, cos_val)
            pred = torch.sigmoid(pred)
            loss = Loss(pred, label)
            ############################################
            
            for pred, gold in zip((pred>=0.5).int(), label.int()):
                if gold == 1 and pred == 1:
                    TP += 1
                elif gold == 0 and pred == 1:
                    FP += 1
                elif gold == 1 and pred == 0:
                    FN += 1

            TP_sum += TP
            FP_sum += FP
            FN_sum += FN
            TP, FP, FN = 0, 0, 0
            tot_loss += loss
            tot_sum += len(label)

    print(TP_sum, FP_sum, FN_sum)
    if TP_sum == 0 and FP_sum == 0:
        prec = 0
    else:
        prec = TP_sum/(TP_sum+FP_sum)
    if TP_sum == 0 and FN_sum == 0:
        recall = 0
    else:
        recall = TP_sum/(TP_sum+FN_sum)
    if TP_sum == 0 and FP_sum == 0 and FN_sum == 0:
        f_score = 0
    else:
        f_score = 2*TP_sum / (2*TP_sum+FP_sum+FN_sum)
    tot_loss = tot_loss/(itera+1)
    print(f"Epoch {epoch}: Loss {tot_loss:.8f}, Prec {prec:.5f}, recall {recall:.5f}, F-Score {f_score:.5f}")

    with open(f'log/{ver}/test_log', 'a') as f:
        f.write(f"Epoch {epoch}: Loss {tot_loss:.8f}, Prec {prec:.5f}, recall {recall:.5f}, F-Score {f_score:.5f}\n")

    if tot_loss < min_loss:
        print("Save Model")
        if f_score > max_f:
            max_f = f_score
        min_loss = tot_loss
        torch.save(NFM_model, f'weights/{ver}/NFM_{epoch}_{f_score:.5f}.pkl')
    elif f_score > max_f:
        print("Save Model")
        max_f = f_score
        torch.save(NFM_model, f'weights/{ver}/NFM_{epoch}_{f_score:.5f}.pkl')