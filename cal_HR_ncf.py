import torch
import torch.nn as nn
import json
from collections import defaultdict
from tqdm import tqdm
from torch.utils.data import DataLoader

from NCF import model, ca_loader

##############
model_pth = 'weights/NCF_enrolled_ver/NCF_80_0.70277.pkl'
##############
###################
Epoch = 200
batch_size = 64
LR = 0.001
weight_lambda = 0.01
Top_K = 10
###################

with open('data/num_to_std.json', 'r') as f:
    num2std = json.load(f)

device = torch.device('cuda')
dataset = ca_loader.CA_HR_NCF_dataset('test')

loader = DataLoader(
    dataset = dataset,
    batch_size = batch_size
)

NCF_model = torch.load(model_pth).to(device)

Loss = nn.BCELoss()

min_loss = 100
max_f = 0

print('Start Evaluation')
NCF_model.eval()

score_res = defaultdict(list)
std_pos_list = defaultdict(list)
with torch.no_grad():
    for itera, (std_idx, cos_idx, label) in enumerate(loader):
        std_idx, cos_idx, label = std_idx.to(device), cos_idx.to(device),\
                label.float().to(device)
        #####################################
        pred = NCF_model(std_idx, cos_idx)
        pred = torch.sigmoid(pred)
        ############################################

        for student, cos, score, pref in zip(std_idx, cos_idx.view(-1).tolist(), pred.view(-1), label):
            student = num2std[str(int(student))]
            score_res[student].append((cos, float(score)))
            if pref == 1:
                std_pos_list[student].append(cos)

    cnt, tot = 0, 0 
    for student in score_res:
        score_res[student].sort(key=lambda x:x[1], reverse=True)
        rs_cos_list = []
        for cos_id, _ in score_res[student][:Top_K]:
            rs_cos_list.append(cos_id)
        ## calculate Hit Ratio
        for gold in std_pos_list[student]:
            tot += 1
            if gold in rs_cos_list:
                cnt += 1
    print(f"Hit Ratio: {cnt/tot}")