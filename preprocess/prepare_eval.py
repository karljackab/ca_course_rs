import func
import pickle
import numpy as np
import json

####################
## Hyper Option
Input_Binary = False
Output_Binary = True
####################

all_student = func.findAllStudent_current()
all_cos = func.findAllCos()

with open('../data/cos_to_num.json', 'r') as f:
    cos2num = json.load(f)

year_sem_list = {
        'year': 108,
        'semester': 2
}

cur_cos = func.findCurCos(year_sem_list)
for idx in range(len(cur_cos)):
    cur_cos[idx] = cos2num[cur_cos[idx]]
score = func.findGradsEval(all_student, all_cos, year_sem_list, cur_cos, Input_Binary, Output_Binary)

if Output_Binary and not Input_Binary:
    name = f'eval_data_binary.pkl'
elif Input_Binary and Output_Binary:
    name = f'eval_data_onehot.pkl'
else:
    name = f'eval_data.pkl'

with open(f'../data/{name}', 'wb') as f:
    pickle.dump(score, f)
print(f'Data Len {len(score)}')