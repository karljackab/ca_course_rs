import MySQLdb
import sqlString as sql
import numpy as np
import re
import random

conn = MySQLdb.connect(db='ca',user='root',passwd='jack02',charset='utf8')

def parseEng(cos):
    cos = cos.strip()
    cos = cos.strip(')')
    cos = cos.lower()
    cos = cos.replace('intro.', 'introduction')
    cos = cos.replace(': ', ' ')
    cos = cos.replace(':', ' ')
    cos = cos.replace('-', ' ')
    cos = cos.replace('/', '')
    cos = cos.replace(', ', ' ')
    cos = cos.replace(',', ' ')
    cos = cos.replace('&', 'and')
    cos = cos.replace(' (', ' ')
    cos = cos.replace('(', ' ')
    cos = cos.replace('lab.', 'lab')
    cos = cos.replace(' 1', ' i')
    cos = cos.replace(' 2', ' ii')

    cos = cos.split(' ')
    while '' in cos:
        cos.remove('')
    cos = ' '.join(cos)
    return cos

def wrong_cos_name_process(name):
    if name == 'problem solving and programming techinques':
        name = 'problem solving and programming techniques'
    elif name == 'android progr':
        name = 'android programming'
    elif name == 'selfexploration and career development':
        name = 'self exploration and career development'
    elif name == 'introduction to hardware software codesign and implementation':
        name = 'introduction to hardware software code sign and implementation'
    elif name == 'viedo compression':
        name = 'video compression'
    elif name == 'complier design':
        name = 'compiler design'
    elif name == 'introduction to embedded systems design and implem':
        name = 'introduction to embedded systems design and implementation'
    elif name == 'embbedded operating system design':
        name = 'embedded operating system design'
    elif name == 'wiredwireless networking infrastructure integrati':
        name = 'wired wireless networking infrastructure integrati'
    elif name == 'mobile applilcation development and project':
        name = 'mobile application development and project'
    elif name == 'embedded systems hardwaresoftware co desi':
        name = 'embedded systems hardware software co-design'
    elif name == 'broadband wireless access networks and applicatio':
        name = 'broadband wireless access networks and applications'
    elif name == 'vehicle positioning electronic maps and integrat':
        name = 'vehicle positioning electronic maps and integrated services'
    elif name == 'nerwork optimization':
        name = 'network optimization'
    return name

def findCurCos(year_sem):
    cursor = conn.cursor()
    cursor.execute(sql.findCurCos, {'prefix': f"{year_sem['year']}-{year_sem['semester']}"})
    temp = cursor.fetchall()
    res = set()
    for i in temp:
        i = i[0]
        i = wrong_cos_name_process(parseEng(i))
        res.add(i)
    res = list(res)
    res.sort()
    cursor.close()
    return res

def findAllCos():
    cursor = conn.cursor()
    cursor.execute(sql.findAllCos)
    temp = cursor.fetchall()
    res = set()
    for i in temp:
        i = i[0]
        i = wrong_cos_name_process(parseEng(i))
        res.add(i)
    res = list(res)
    res.sort()
    cursor.close()
    return res

def findAllStudent_current():
    cursor = conn.cursor()
    cursor.execute("select distinct student_id from student")
    temp = cursor.fetchall()
    res=set()
    for i in temp:
        ## Filter student who are not undergraduate student
        i = i[0]
        if i[0] != '0' or i[2] != '1':
            continue
        res.add(i)
    res = list(res)
    res.sort()
    cursor.close()
    return res

def findAllStudent_fromScore():
    cursor = conn.cursor()
    cursor.execute("select distinct student_id from cos_score")
    temp = cursor.fetchall()
    res=set()
    for i in temp:
        ## Filter student who are not undergraduate student
        i = i[0]
        if i[0] != '0' or i[2] != '1':
            continue
        res.add(i)
    res = list(res)
    res.sort()
    cursor.close()
    return res

def normByCos(s, avg_std):
    new_s = (s-avg_std['avg'])/avg_std['std']
    return new_s

def normByStudent(s, score):
    new_s = (s-score.mean())/score.std()
    return new_s

def findGradsAvgStd():
    cursor = conn.cursor()
    cursor.execute(sql.findGradAvgStd)
    temp = cursor.fetchall()
    res = dict()
    for data in temp:
        res[data[0]] = {
            'avg': data[1], 
            'std': data[2]
        }
    cursor.close()
    return res

def findGradsEval(stds, cos, test_list, cur_cos, Input_Binary=False, Output_Binary=False):
    cursor = conn.cursor()
    res = []
    avg_std_map = findGradsAvgStd()
    for std in stds:
        score_list = []

        ### Current Student's grade
        cursor.execute(sql.findStdGrade, {'id': std, 'year': test_list['year'],\
             'semester': test_list['semester']})
        sgrade = cursor.fetchall()[0][0]

        ### Remaining Semester Score
        cursor.execute(sql.findGradExclude, {'id': std, 'year': test_list['year'],\
             'semester': test_list['semester']})
        temp = cursor.fetchall()
        if len(temp) == 0:
            continue

        grad = np.full(len(cos), np.nan)
        cos_order = []
        cos_pos_idx = []
        pre_sem = ''
        cur_record_idx = -1
        for data in temp:
            name = wrong_cos_name_process(parseEng(data[1]))
            idx = cos.index(name)
            if data[2] == None or avg_std_map[data[0]]['std'] == 0:
                continue
            grad[idx] = normByCos(data[2], avg_std_map[data[0]])
            score_list.append(grad[idx])
            cos_order.append(idx)

            cur_sem = '-'.join(data[0].split('-')[:-1])
            if pre_sem != cur_sem:
                pre_sem = cur_sem
                cur_record_idx += 1
            cos_pos_idx.append(cur_record_idx)
        ###################################
        ### Student Intra Normalization
        score_list = np.array(score_list)
        if score_list.std() == 0 or np.isnan(score_list.std()):
            continue
        grad = normByStudent(grad, score_list)
        if Input_Binary:
            for idx, i in enumerate(grad):
                if i > 0:
                    grad[idx] = 1
                elif i < 0:
                    grad[idx] = -1
        ###################################
        res.extend([{
            'std_id': std,
            'sgrade': sgrade,
            'data': np.nan_to_num(grad),
            'record_order': cos_order,
            'cos_idx': cos_idx,
            'cos_pos_idx': cos_pos_idx
        } for cos_idx in cur_cos])

    return res

def findGrads_HR(stds, cos, test_list, cur_cos, Input_Binary=False, Output_Binary=False):
    cursor = conn.cursor()
    res = []
    avg_std_map = findGradsAvgStd()
    for std in stds:
        score_list = []

        ### Current Student's grade
        cursor.execute(sql.findStdGrade, {'id': std, 'year': test_list['year'],\
             'semester': test_list['semester']})
        sgrade = cursor.fetchall()[0][0]
        ################
        ### Specific Semester Score
        cursor.execute(sql.findGradSpecify, {'id':std, 'year': test_list['year'],\
             'semester': test_list['semester']})
        temp = cursor.fetchall()
        if len(temp) == 0:
            continue

        test_grad = np.full(len(cos), np.nan)
        for data in temp:
            name = wrong_cos_name_process(parseEng(data[1]))
            idx = cos.index(name)
            if data[2] == None or avg_std_map[data[0]]['std'] == 0:
                continue
            test_grad[idx] = normByCos(data[2], avg_std_map[data[0]])
            score_list.append(test_grad[idx])
        ################
        ### Remaining Semester Score
        cursor.execute(sql.findGradExclude, {'id': std, 'year': test_list['year'],\
             'semester': test_list['semester']})
        temp = cursor.fetchall()
        if len(temp) == 0:
            continue

        grad = np.full(len(cos), np.nan)
        cos_order = []
        cos_pos_idx = []
        pre_sem = ''
        cur_record_idx = -1
        for data in temp:
            name = wrong_cos_name_process(parseEng(data[1]))
            idx = cos.index(name)
            if data[2] == None or avg_std_map[data[0]]['std'] == 0:
                continue
            grad[idx] = normByCos(data[2], avg_std_map[data[0]])
            score_list.append(grad[idx])
            cos_order.append(idx)

            cur_sem = '-'.join(data[0].split('-')[:-1])
            if pre_sem != cur_sem:
                pre_sem = cur_sem
                cur_record_idx += 1
            cos_pos_idx.append(cur_record_idx)
        ###################################
        ### Student Intra Normalization
        score_list = np.array(score_list)
        if score_list.std() == 0 or np.isnan(score_list.std()):
            continue
        grad = normByStudent(grad, score_list)
        test_grad = normByStudent(test_grad, score_list)
        target_idx_list = []
        target_pref_list = dict()
        if Output_Binary:
            for idx, i in enumerate(test_grad):
                if i > 0:
                    test_grad[idx] = 1
                    target_idx_list.append(idx)
                    target_pref_list[idx] = 1
                elif i < 0:
                    test_grad[idx] = -1
                    target_idx_list.append(idx)
                    target_pref_list[idx] = 0
        if Input_Binary:
            for idx, i in enumerate(grad):
                if i > 0:
                    grad[idx] = 1
                elif i < 0:
                    grad[idx] = -1
        ###################################
        for cos_idx in cur_cos:
            ## introduction to compiler design
            ## introduction to formal language
            ## introduction to computer science
            if cos_idx in [144, 158]:
                continue
            if cos_idx not in target_idx_list or target_pref_list[cos_idx] == 0:
                cos_pref = 0
            else:
                cos_pref = 1
            res.append({
                'std_id': std,
                'sgrade': sgrade,
                'data': np.nan_to_num(grad),
                'record_order': cos_order,
                'cos_idx': cos_idx,
                'cos_pref': cos_pref,
                'cos_pos_idx': cos_pos_idx
            })

    return res

def findGrads_NCF(stds, cos2num, test_year):
    cursor = conn.cursor()
    res = []
    avg_std_map = findGradsAvgStd()
    for std in stds:
        score_list = []

        ### Specific Semester Score
        cursor.execute(sql.findGradSpecify_year, {'id':std, 'year': test_year})
        temp = cursor.fetchall()
        if len(temp) == 0:
            continue

        test_grad = np.full(len(cos2num), np.nan)
        for data in temp:
            name = wrong_cos_name_process(parseEng(data[1]))
            idx = cos2num[name]
            if data[2] == None or avg_std_map[data[0]]['std'] == 0:
                continue
            test_grad[idx] = normByCos(data[2], avg_std_map[data[0]])
            score_list.append(test_grad[idx])
        ################
        ### Remaining Semester Score
        cursor.execute(sql.findGradExclude_year, {'id': std, 'year': test_year})
        temp = cursor.fetchall()
        if len(temp) == 0:
            continue

        grad = np.full(len(cos2num), np.nan)
        for data in temp:
            name = wrong_cos_name_process(parseEng(data[1]))
            idx = cos2num[name]
            if data[2] == None or avg_std_map[data[0]]['std'] == 0:
                continue
            grad[idx] = normByCos(data[2], avg_std_map[data[0]])
            score_list.append(grad[idx])
        ###################################
        ### Student Intra Normalization
        score_list = np.array(score_list)
        if score_list.std() == 0 or np.isnan(score_list.std()):
            continue
        grad = normByStudent(grad, score_list)
        test_grad = normByStudent(test_grad, score_list)
        ###################################
        res.append({
            'std_id': std,
            'train': np.nan_to_num(grad),
            'test': np.nan_to_num(test_grad),
        })

    return res

def transform_to_prob(x):
    return np.min([np.nan_to_num(1/(1+np.exp(-x)) / 0.8), 1.]) ## sigmoid function / 0.8

def findGrads(stds, cos, test_list, Input_Binary=False, Output_Binary=True, all_cos=False, train=False, test=False):
    cursor = conn.cursor()
    res = []
    avg_std_map = findGradsAvgStd()

    positive_cnt = 0
    negative_cnt = 0
    cur_cos = [cos.index(name) for name in findCurCos(test_list)]
    # print(cur_cos)
    for std in stds:
        score_list = []

        ################
        ### Current Student's grade
        cursor.execute(sql.findStdGrade, {'id': std, 'year': test_list['year'],\
             'semester': test_list['semester']})
        sgrade = cursor.fetchall()[0][0]
        ################
        ### Specific Semester Score
        cursor.execute(sql.findGradSpecify, {'id':std, 'year': test_list['year'],\
             'semester': test_list['semester']})
        temp = cursor.fetchall()
        if len(temp) == 0:
            continue

        test_grad = np.full(len(cos), np.nan)
        for data in temp:
            name = wrong_cos_name_process(parseEng(data[1]))
            idx = cos.index(name)
            if data[2] == None or avg_std_map[data[0]]['std'] == 0:
                continue
            test_grad[idx] = normByCos(data[2], avg_std_map[data[0]])
            score_list.append(test_grad[idx])
        ################
        ### Remaining Semester Score
        cursor.execute(sql.findGradExclude, {'id': std, 'year': test_list['year'],\
             'semester': test_list['semester']})
        temp = cursor.fetchall()
        if len(temp) == 0:
            continue

        grad = np.full(len(cos), np.nan)
        cos_order = []
        cos_pos_idx = []
        pre_sem = ''
        cur_record_idx = -1
        for data in temp:
            name = wrong_cos_name_process(parseEng(data[1]))
            idx = cos.index(name)
            if data[2] == None or avg_std_map[data[0]]['std'] == 0:
                continue
            grad[idx] = normByCos(data[2], avg_std_map[data[0]])
            score_list.append(grad[idx])
            cos_order.append(idx)

            cur_sem = '-'.join(data[0].split('-')[:-1])
            if pre_sem != cur_sem:
                pre_sem = cur_sem
                cur_record_idx += 1
            cos_pos_idx.append(cur_record_idx)
        ###################################
        ### Student Intra Normalization
        score_list = np.array(score_list)
        if score_list.std() == 0 or np.isnan(score_list.std()):
            continue
        grad = normByStudent(grad, score_list)
        test_grad = normByStudent(test_grad, score_list)
        target_idx_list = []
        target_pref_list = []

        for idx, i in enumerate(test_grad):
            # i could be nan or positive or negative
            if idx in [144, 158, 148, 149, 150]:
                continue
            if idx not in cur_cos:
                continue

            if test == True:
                if i > 0:
                    test_grad[idx] = 1
                    target_idx_list.append(idx)
                    if Output_Binary:
                        target_pref_list.append(1)
                    else:
                        target_pref_list.append(transform_to_prob(i))
                    positive_cnt += 1
                # elif random.random() < 0.3:
                else:
                    test_grad[idx] = -1
                    target_idx_list.append(idx)
                    if Output_Binary:
                        target_pref_list.append(0)
                    else:
                        target_pref_list.append(transform_to_prob(i))
                    negative_cnt += 1
            elif train == True:
                if i > 0:
                    test_grad[idx] = 1
                    target_idx_list.append(idx)
                    if Output_Binary:
                        target_pref_list.append(1)
                    else:
                        target_pref_list.append(transform_to_prob(i))
                    positive_cnt += 1
                ## 164 introduction to multimedia information system
                ##  79 differential equations
                ## 161 introduction to internet of things
                ## 101 elliptic curve cryptography
                ## 154 introduction to database systems
                ## 142 introduction to artificial intelligence
                ## 151 introduction to computer security
                ## 234 principles of communications networks
                ## 160 introduction to image processing
                ## 211 network security
                # elif random.random() < 0.007 or idx in [164, 79, 161, 101, 154, 142, 151, 234, 160, 211]:
                else:
                    test_grad[idx] = -1
                    target_idx_list.append(idx)
                    if Output_Binary:
                        target_pref_list.append(0)
                    else:
                        target_pref_list.append(transform_to_prob(i))
                    negative_cnt += 1
            else:
                if i > 0:
                    test_grad[idx] = 1
                    target_idx_list.append(idx)
                    if Output_Binary:
                        target_pref_list.append(1)
                    else:
                        target_pref_list.append(transform_to_prob(i))
                    positive_cnt += 1
                # elif i < 0:
                else:
                    test_grad[idx] = -1
                    target_idx_list.append(idx)
                    if Output_Binary:
                        target_pref_list.append(0)
                    else:
                        target_pref_list.append(transform_to_prob(i))
                    negative_cnt += 1      

        # if Input_Binary:
        #     for idx, i in enumerate(grad):
        #         if i > 0:
        #             grad[idx] = 1
        #         elif i < 0:
        #             grad[idx] = -1
        ###################################
        res.extend([{
            'std_id': std,
            'sgrade': sgrade,
            'data': np.nan_to_num(grad),
            'record_order': cos_order,
            'cos_idx': cos_idx,
            'cos_pref': cos_pref,
            'cos_pos_idx': cos_pos_idx
            # 'gold': np.nan_to_num(test_grad)
        } for cos_idx, cos_pref in zip(target_idx_list, target_pref_list)])
    print(f'Positive Count {positive_cnt}, Negative Count {negative_cnt}')
    return res