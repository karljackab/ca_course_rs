import pickle
import numpy as np
import json

train = []
for i in range(103, 107):
    train.append([i, 1])
    train.append([i, 2])
train.append([107, 1])
train_data = []

for year, semester in train:
    print(f'{year}-{semester}')
    with open(f'../data/{year}-{semester}_whole_prob.pkl', 'rb') as f:
        data = pickle.load(f)
        train_data.extend(data)
with open('../data/train_data_whole_prob.pkl', 'wb') as f:
    pickle.dump(train_data, f)
print(f'Train Len {len(train_data)}')