import func
import pickle
import numpy as np
import json
import matplotlib.pyplot as plt

##############Visualize Enrolled Chart########################
def Visualize(score):
    with open('../data/num_to_cos.json', 'r') as f:
        num2cos = json.load(f)
    res_cnt = [0]*311
    std_id = set()
    tot_cnt = 0
    for item in score:
        if item['cos_pref'] >= 0.5:
            std_id.add(item['std_id'])
            res_cnt[item['cos_idx']] += 1
            tot_cnt += 1
    res_cnt = np.array(res_cnt)
    res_order = res_cnt.argsort()[::-1]
    res_order = res_order[res_cnt[res_order]!=0]
    label = []
    for idx in res_order[:10]:
        print(num2cos[str(idx)])
        print(res_cnt[idx])
        label.append(num2cos[str(idx)])
    plt.bar(range(10), res_cnt[res_order][:10], tick_label=label)
    plt.xticks(rotation=10)
    plt.title(f"{year_sem_list['year']}-{year_sem_list['semester']}(Student number: {len(std_id)})")
    plt.ylabel('Student Counts')
    plt.show()
    # plt.savefig(f"statistic/{year_sem_list['year']}-{year_sem_list['semester']}.png")

####################
###### Data Format
### {
###     'std_id': std,
###     'data': grad,
###     'gold': test_grad
### }
####################
## Hyper Option
Input_Binary = False
Output_Binary = False
AllCos = True
####################

all_student = func.findAllStudent_fromScore()
all_cos = func.findAllCos()

with open('../data/cos_to_num.json', 'w', encoding='UTF-8') as f1,\
     open('../data/num_to_cos.json', 'w', encoding='UTF-8') as f2:
    cos_to_num = {}
    num_to_cos = {}
    for i in range(len(all_cos)):
        cos_to_num[all_cos[i]] = i
        num_to_cos[i] = all_cos[i]
    json.dump(cos_to_num, f1, indent=4)
    json.dump(num_to_cos, f2, indent=4)

test = [[107, 2]]
test_data = []
for year, semester in test:
    year_sem_list = {
        'year': year,
        'semester': semester
    }
    score = func.findGrads(all_student, all_cos, year_sem_list,\
         Input_Binary, Output_Binary, all_cos=AllCos, test=True)
    test_data.extend(score)

if AllCos and Output_Binary:
    name = 'test_data_all.pkl'
elif AllCos and not Output_Binary:
    name = 'test_data_all_prob.pkl'
elif Output_Binary and not Input_Binary:
    name = 'test_data_binary.pkl'
elif Input_Binary and Output_Binary:
    name = 'test_data_onehot.pkl'
else:
    name = 'test_data.pkl'
name = 'test_data_whole_prob.pkl'
with open(f'../data/{name}', 'wb') as f:
    pickle.dump(test_data, f)
print(f'Test Len {len(test_data)}')

###################
train = []
for i in range(103, 107):
    train.append([i, 1])
    train.append([i, 2])
train.append([107, 1])
train_data = []

for year, semester in train:
    print(f'{year}-{semester}')
    year_sem_list = {
        'year': year,
        'semester': semester
    }
    score = func.findGrads(all_student, all_cos, year_sem_list,\
         Input_Binary, Output_Binary, all_cos=AllCos, train=True)

    with open(f'../data/{year}-{semester}_whole_prob.pkl', 'wb') as f:
        pickle.dump(score, f)
    # Visualize(score)
    print(f'Train Len {len(score)}')

    del score

    # train_data.extend(score)

# if AllCos and Output_Binary:
#     name = 'train_data_all.pkl'
# elif AllCos and not Output_Binary:
#     name = 'train_data_all_prob.pkl'
# elif Output_Binary and not Input_Binary:
#     name = 'train_data_binary.pkl'
# elif Input_Binary and Output_Binary:
#     name = 'train_data_onehot.pkl'
# else:
#     name = 'train_data.pkl'

# with open(f'../data/{name}', 'wb') as f:
#     pickle.dump(train_data, f)
# print(f'Train Len {len(train_data)}')
# Visualize(train_data)