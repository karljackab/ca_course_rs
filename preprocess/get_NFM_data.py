import pickle
import numpy as np
import json

with open('../data/test_data_whole_prob.pkl', 'rb') as f1,\
    open('../data/train_data_whole_prob.pkl', 'rb') as f2:
    test_data = pickle.load(f1)
    train_data = pickle.load(f2)

test_res = []
for item in test_data:
    sid = item['std_id']
    cos_idxes = np.arange(item['data'].shape[0]).tolist()
    cos_val = item['data'].tolist()

    cos_idxes += [item['cos_idx']]
    cos_val += [2.]
    label = 1. if item['cos_pref'] > 0 else 0
    test_res.append({
        'std_id': sid,
        'cos_idxes': cos_idxes,
        'cos_val': cos_val,
        'label': label
    })

with open('../data/NFM_test_data.json', 'w') as f:
    json.dump(test_res, f, indent=4)

train_res = []
for item in train_data:
    sid = item['std_id']
    cos_idxes = np.arange(item['data'].shape[0]).tolist()
    cos_val = item['data'].tolist()

    cos_idxes += [item['cos_idx']]
    cos_val += [2.]
    label = 1. if item['cos_pref'] > 0 else 0
    train_res.append({
        'std_id': sid,
        'cos_idxes': cos_idxes,
        'cos_val': cos_val,
        'label': label
    })

with open('../data/NFM_train_data.json', 'w') as f:
    json.dump(train_res, f, indent=4)