import func
import pickle
import numpy as np
import json
from collections import defaultdict
import matplotlib.pyplot as plt

all_student = func.findAllStudent_fromScore()

with open('../data/cos_to_num.json', 'r') as f:
    cos2num = json.load(f)
with open('../data/std_to_num.json', 'r') as f1,\
        open('../data/num_to_std.json', 'r') as f2:
    std2num = json.load(f1)
    num2std = json.load(f2)

data_year = 107
data_type = 'test_NCF_HR' ## eval, test_HR, train_HR
query_res = func.findGrads_NCF(all_student, cos2num, data_year)

## Parsing train and test data\
res = []
for item in query_res:
    data = item['test']
    std_id = item['std_id']
    if len(data[data!=0]) == 0:
        continue
    for cos_idx, score in enumerate(data.tolist()):
        if score > 0:
            res.append((std_id, cos_idx, 1)) ## std_id, cos_idx, label
        else:
            res.append((std_id, cos_idx, 0))

## Save Result
with open(f'../data/{data_type}.json', 'w') as f:
    json.dump(res, f, indent=4)
print(f"Data Len {len(res)}")