import func
import pickle
import numpy as np
import json

all_student = func.findAllStudent_fromScore()

with open('../data/cos_to_num.json', 'r') as f:
    cos2num = json.load(f)
with open('../data/std_to_num.json', 'w') as f1,\
        open('../data/num_to_std.json', 'w') as f2:
    std2num = dict()
    num2std = dict()
    for std_id in all_student:
        std2num[std_id] = len(std2num)
        num2std[std2num[std_id]] = std_id
    json.dump(std2num, f1, indent=4)
    json.dump(num2std, f2, indent=4)

test_year = 107
####
## Data Format
## [{
##     'std_id': std,
##     'train': np.nan_to_num(grad),
##     'test': np.nan_to_num(test_grad),
## }, {} ]
####
query_res = func.findGrads_NCF(all_student, cos2num, test_year)

## Parsing train and test data
res = dict()
for mode in ['train', 'test']:
    temp_res = []
    for item in query_res:
        data = item[mode]
        std_id = item['std_id']
        if len(data[data!=0]) == 0:
            continue
        for cos_idx, score in enumerate(data.tolist()):
            if score < 0:
                temp_res.append((std_id, cos_idx, 0)) ## std_id, cos_idx, label
            elif score > 0:
                temp_res.append((std_id, cos_idx, 1))
    res[mode] = temp_res

## Save Result
with open(f'../data/NCF_data_enroll_ver.json', 'w') as f:
    json.dump(res, f, indent=4)
print(f"Train Len {len(res['train'])}, Test Len {len(res['test'])}")