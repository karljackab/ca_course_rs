import func
import pickle
import numpy as np
import json
from collections import defaultdict
import csv

####################
###### Data Format
### {
###     'std_id': std,
###     'data': grad,
###     'gold': test_grad
### }
####################
## Hyper Option
Input_Binary = False
Output_Binary = True
####################

all_student = func.findAllStudent_fromScore()
all_cos = func.findAllCos()

test = [[107, 1]]
test_data = []
for year, semester in test:
    year_sem_list = {
        'year': year,
        'semester': semester
    }
    score = func.findGrads(all_student, all_cos, year_sem_list, Input_Binary, Output_Binary)

res = defaultdict(list)
for item in score:
    res[item['std_id']].append(item['cos_idx'])
with open('temp.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    for key in res:
        content = [key] + res[key]
        writer.writerow(content)