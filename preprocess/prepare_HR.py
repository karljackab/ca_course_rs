import func
import pickle
import numpy as np
import json
from collections import defaultdict
import matplotlib.pyplot as plt

####################
## Hyper Option
Input_Binary = False
Output_Binary = True
####################

all_student = func.findAllStudent_fromScore()
all_cos = func.findAllCos()

with open('../data/cos_to_num.json', 'r') as f:
    cos2num = json.load(f)

year_sem_list = {
        'year': 105,
        'semester': 1
}
data_type = 'test_HR' ## eval, test_HR, train_HR

cur_cos = func.findCurCos(year_sem_list)
for idx in range(len(cur_cos)):
    cur_cos[idx] = cos2num[cur_cos[idx]]
score = func.findGrads_HR(all_student, all_cos, year_sem_list, cur_cos, Input_Binary, Output_Binary)

# if Output_Binary and not Input_Binary:
#     name = f'{data_type}_data_binary.pkl'
# elif Input_Binary and Output_Binary:
#     name = f'{data_type}_data_onehot.pkl'
# else:
#     name = f'{data_type}_data.pkl'

# with open(f'../data/{name}', 'wb') as f:
#     pickle.dump(score, f)
# print(f'Data Len {len(score)}')

def Visualize(score):
    ##############Visualize Enrolled Chart########################
    with open('../data/num_to_cos.json', 'r') as f:
        num2cos = json.load(f)
    res_cnt = [0]*311
    std_id = set()
    tot_cnt = 0
    for item in score:
        if item['cos_pref'] == 1:
            std_id.add(item['std_id'])
            res_cnt[item['cos_idx']] += 1
            tot_cnt += 1
    res_cnt = np.array(res_cnt)
    res_order = res_cnt.argsort()[::-1]
    res_order = res_order[res_cnt[res_order]!=0]
    # print(res_order)
    print(res_cnt[res_order])
    print(len(std_id))
    print(tot_cnt)
    print(num2cos[str(res_order[0])])
    label = []
    for idx in res_order[:10]:
        label.append(num2cos[str(idx)])
    plt.bar(range(10), res_cnt[res_order][:10], tick_label=label)
    plt.xticks(rotation=10)
    plt.title(f"{year_sem_list['year']}-{year_sem_list['semester']}(Student number: {len(std_id)})")
    plt.ylabel('Student Counts')
    plt.show()
    # plt.savefig(f"statistic/{year_sem_list['year']}-{year_sem_list['semester']}.png")