import pickle
import numpy as np
import json
import matplotlib.pyplot as plt

Top_K = 100

def Visualize(score):
    with open('../data/num_to_cos.json', 'r') as f:
        num2cos = json.load(f)
    res_cnt = [0]*311
    std_id = set()
    tot_cnt = 0
    for item in score:
        if item['cos_pref'] >= 0.5:
            std_id.add(item['std_id'])
            res_cnt[item['cos_idx']] += 1
            tot_cnt += 1
    res_cnt = np.array(res_cnt)
    res_order = res_cnt.argsort()[::-1]
    res_order = res_order[res_cnt[res_order]!=0]
    # label = []
    for idx in res_order[:Top_K]:
        print(num2cos[str(idx)], res_cnt[idx])
        # label.append(num2cos[str(idx)])
    # plt.bar(range(Top_K), res_cnt[res_order][:Top_K], tick_label=label)
    plt.bar(range(Top_K), res_cnt[res_order][:Top_K])
    plt.xticks(rotation=10)
    plt.title(f"Train Statistic (Student number: {len(std_id)})")
    plt.ylabel('Student Counts')
    plt.show()

train_pth = '../data/train_data_whole_prob.pkl'
test_pth = '../data/test_data_whole_prob.pkl'

with open(train_pth, 'rb') as f:
    data = pickle.load(f)
Visualize(data)