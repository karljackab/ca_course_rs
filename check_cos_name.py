import os
import torch
import torch.nn as nn
import models.dataloader as loader
from torch.utils.data import DataLoader

import models.course as course
import models.student as student

###################
Epoch = 200
batch_size = 64
LR = 0.0005
weight_lambda = 0.01
show_iter = 10
dup_threshold = 15
dup_weight = 0.01
###################

device = torch.device('cuda')

train_dataset = loader.CA_dataset('train')
test_dataset = loader.CA_dataset('test')

def loader_collatfn(data):
    record, cos_idx, cos_pref, record_order, sgrade, cos_pos_idx = [], [], [], [], [], []
    for item in data:
        record.append(item[0])
        cos_idx.append(item[2])
        cos_pref.append(item[3])
        record_order.append(torch.tensor(item[1]).to(device))
        sgrade.append(item[4])
        cos_pos_idx.append(item[5].unsqueeze(1).to(device))
    record = torch.stack(record, dim=0)
    cos_idx = torch.stack(cos_idx, dim=0).view(-1, 1)
    cos_pref = torch.stack(cos_pref, dim=0)
    sgrade = torch.tensor(sgrade).float().view(-1, 1)
    return record.to(device), record_order, cos_idx.to(device),\
         cos_pref.to(device), sgrade.to(device), cos_pos_idx

train_data = DataLoader(
    dataset = train_dataset,
    batch_size = batch_size,
    shuffle = True,
    collate_fn=loader_collatfn
)
test_data = DataLoader(
    dataset = test_dataset,
    batch_size = batch_size,
    collate_fn=loader_collatfn
)

Cos_name_model = course.Cos_embed(device).to(device)
Course_embedding = course.Course_embedding_whole().to(device)
Student_embedding = student.Student_embedding_whole().to(device)

min_loss = 100
max_f = 0
Cos_name_model.train()
Course_embedding.train()
Student_embedding.train()
tot_len = len(train_data)
for itera, (record, record_order, cos_idx, cos_pref, sgrade, cos_pos_idx) in enumerate(train_data):
    #####################################
    print(f'{itera}/{tot_len}')
    # Student_embedding(record, record_order, Cos_name_model, sgrade, cos_pos_idx)
    Course_embedding(cos_idx, Cos_name_model)
    