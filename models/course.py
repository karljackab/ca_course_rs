import torch
import torch.nn as nn
import gensim
import json

class Cos_embed(nn.Module):
    def __init__(self, device):
        super().__init__()
        glove_dim = 100
        print('========')
        print(f'Loading Glove-{glove_dim} Embedding')
        
        model = gensim.models.KeyedVectors.load_word2vec_format(f'data/glove.6B.{glove_dim}d.txt')
        glove_weight = torch.FloatTensor(model.vectors)
        self.embedding = nn.Embedding.from_pretrained(glove_weight)
        print('Load Successfully')
        print('========')

        # self.hidden_dim = 50
        # self.RNN = nn.LSTM(50, self.hidden_dim, bidirectional=True, batch_first=True)
        
        # ## Distribution Version
        # self.linear = nn.Linear(self.hidden_dim*4, 100)

        ## Dot Product Version
        # self.linear = nn.Sequential(
        #     nn.Linear(self.hidden_dim*4, 100)
        # )

        ## All Version
        self.hidden_dim = 100
        # self.hidden_dim = 200
        self.RNN = nn.LSTM(100, self.hidden_dim, bidirectional=True, batch_first=True)
        # self.linear = nn.Sequential(
        #     nn.Linear(self.hidden_dim*4, 500),
        #     nn.ReLU(),
        #     nn.Linear(500, 300),
        # )
        self.linear = nn.Sequential(
            nn.Linear(self.hidden_dim*4, 200),
            nn.ReLU(),
            nn.Linear(200, 100),
        )
        
        with open('data/word2idx.json', 'rb') as f:
            self.word2idx = json.load(f)
        with open('data/idx2word.json', 'rb') as f:
            self.idx2word = json.load(f)
        with open('data/num_to_cos.json', 'r') as f:
            self.idx2cos = json.load(f)

        self.device = device

    def embed(self, cos_name):
        cos_word_idx = [self.word2idx[name] for name in cos_name]
        cos_embedding = self.embedding(torch.tensor(cos_word_idx).to(self.device))
        return cos_embedding

    def forward(self, cos_idx_list):
        cos_name_list = [self.idx2cos[str(int(cos_idx))].split(' ') for cos_idx in cos_idx_list]
        cos_embedding = [self.embed(cos_name) for cos_name in cos_name_list]
        cos_embedding = nn.utils.rnn.pad_sequence(cos_embedding, batch_first=True)
        
        cos_name_len = [len(cos_name) for cos_name in cos_name_list]
        ## enforce_sorted=False??
        cos_embedding = nn.utils.rnn.pack_padded_sequence(cos_embedding, cos_name_len, batch_first=True, enforce_sorted=False)
        
        _, (h, c) = self.RNN(cos_embedding)
        hidden = torch.cat((h[0,:,:].view(-1, self.hidden_dim), h[1,:,:].view(-1, self.hidden_dim),\
             c[0,:,:].view(-1, self.hidden_dim), c[1,:,:].view(-1, self.hidden_dim)), dim=1)
        emb_res = self.linear(hidden)
        return emb_res

class Course_embedding_whole(nn.Module):
    def __init__(self):
        super().__init__()
        ## All Version
        self.linear_pre = nn.Sequential(
            nn.ReLU(),
            nn.Linear(100, 70),
            nn.Dropout(p=0.3),
            nn.ReLU(),
            nn.Linear(70, 50),
            nn.Dropout(p=0.1),
            nn.ReLU(),
            nn.Linear(50, 30),
        )
        # self.linear_pre = nn.Sequential(
        #     nn.ReLU(),
        #     nn.Linear(300, 200),
        #     nn.Dropout(p=0.3),
        #     nn.ReLU(),
        #     nn.Linear(200, 100),
        #     nn.Dropout(p=0.1),
        #     nn.ReLU(),
        #     nn.Linear(100, 50),
        # )
        ##############################################
        ## Dot Product Version
        # self.linear_pre = nn.Sequential(
        #     nn.ReLU(),
        #     nn.Linear(100, 30),
        # )
        ##############################################
        ## Distribution Version
        # self.linear_pre = nn.Sequential(
        #     nn.ReLU(),
        #     nn.Linear(100, 50),
        #     nn.Dropout(p=0.5),
        #     nn.ReLU(),
        # )
        # self.mu_linear = nn.Sequential(
        #     nn.Linear(50, 40),
        #     nn.Dropout(p=0.1),
        #     nn.ReLU(),
        #     nn.Linear(40, 30),
        #     nn.ReLU()
        # )
        # self.sigma_linear = nn.Sequential(
        #     nn.Linear(50, 40),
        #     nn.Dropout(p=0.1),
        #     nn.ReLU(),
        #     nn.Linear(40, 30),
        #     nn.ReLU()
        # )

    def forward(self, cos_idx_list, cos_embedding_model):
        embedding = cos_embedding_model(cos_idx_list)
        temp = self.linear_pre(embedding)
        # mu = self.mu_linear(temp)
        # sigma = self.sigma_linear(temp)
        return temp
        # return mu, sigma