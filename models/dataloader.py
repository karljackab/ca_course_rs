from torch.utils.data.dataset import Dataset
import pickle
import torch
import json

class CA_dataset(Dataset):
    def __init__(self, mode, prob=False):
        self.mode = mode
        self.prob = prob
        if self.mode == 'train':
            pth = 'data/train_data_all.pkl'
            if prob:
                print('Prob Score')
                # pth = 'data/train_data_all_prob.pkl'
                pth = 'data/train_data_whole_prob.pkl'
        elif self.mode == 'test':
            pth = 'data/test_data_all.pkl'
            if prob:
                print('Prob Score')
                # pth = 'data/test_data_all_prob.pkl'
                pth = 'data/test_data_whole_prob.pkl'
        
        with open(pth, 'rb') as f:
            self.datas = pickle.load(f)

    def __len__(self):
        return len(self.datas)

    def __getitem__(self, idx):
        sid = self.datas[idx]['std_id']
        sgrade = self.datas[idx]['sgrade']
        record = torch.tensor(self.datas[idx]['data'])
        record_order = self.datas[idx]['record_order']
        cos_idx = torch.tensor(self.datas[idx]['cos_idx'])
        cos_pos_idx = torch.tensor(self.datas[idx]['cos_pos_idx'])
        if self.prob:
            cos_pref = torch.tensor(self.datas[idx]['cos_pref']).float()
        else:
            if self.datas[idx]['cos_pref'] == 1:
                cos_pref = torch.tensor(1.)
            else:
                cos_pref = torch.tensor(0.)
        return sid, record, record_order, cos_idx, cos_pref, sgrade, cos_pos_idx

class CA_eval_dataset(Dataset):
    def __init__(self, data_type=None):
        self.data_type = data_type
        if data_type == 'test_HR':
            pth = 'data/test_HR_data_binary.pkl'
        elif data_type == 'train_HR':
            pth = 'data/train_HR_data_binary.pkl'
        else:
            pth = 'data/eval_data_binary.pkl'

        with open(pth, 'rb') as f:
            self.datas = pickle.load(f)

    def __len__(self):
        return len(self.datas)

    def __getitem__(self, idx):
        sid = self.datas[idx]['std_id']
        sgrade = self.datas[idx]['sgrade']
        record = torch.tensor(self.datas[idx]['data'])
        record_order = self.datas[idx]['record_order']
        cos_idx = torch.tensor(self.datas[idx]['cos_idx'])
        cos_pos_idx = torch.tensor(self.datas[idx]['cos_pos_idx'])
        if self.data_type:
            cos_pref = self.datas[idx]['cos_pref']
            return sid, record, record_order, cos_idx, sgrade, cos_pos_idx, cos_pref
        else:
            return sid, record, record_order, cos_idx, sgrade, cos_pos_idx