import torch
import torch.nn as nn
import json
from torch.nn import TransformerEncoder, TransformerEncoderLayer
import math

class PositionalEncoding(nn.Module):
    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        div_term = torch.exp(torch.arange(0, d_model).float() * (-math.log(10000.0) / d_model))
        self.register_buffer('div_term', div_term)

    def forward(self, x, cos_pos_idx):
        cos_pos_idx = torch.sin(cos_pos_idx * self.div_term).unsqueeze(0)
        x = x + cos_pos_idx
        return self.dropout(x)

class TransformerModel(nn.Module):
    def __init__(self, ninp, nhead, nhid, nlayers, dropout=0.3):
        super(TransformerModel, self).__init__()
        self.src_mask = None
        self.pos_encoder = PositionalEncoding(ninp, dropout)
        encoder_layers = TransformerEncoderLayer(ninp, nhead, nhid, dropout)
        self.transformer_encoder = TransformerEncoder(encoder_layers, nlayers)
        self.ninp = ninp

    def _generate_square_subsequent_mask(self, sz):
        mask = (torch.triu(torch.ones(sz, sz)) == 1).transpose(0, 1)
        mask = mask.float().masked_fill(mask == 0, float('-inf')).masked_fill(mask == 1, float(0.0))
        return mask

    def forward(self, src, cos_pos_idx):
        if self.src_mask is None or self.src_mask.size(0) != len(src):
            device = src.device
            mask = self._generate_square_subsequent_mask(len(src)).to(device)
            self.src_mask = mask

        src = src * math.sqrt(self.ninp)
        src = self.pos_encoder(src, cos_pos_idx)
        output = self.transformer_encoder(src, self.src_mask)
        return output


class Student_embedding_whole(nn.Module):
    def __init__(self):
        super().__init__()
        # self.trans_enc = TransformerModel(100, 2, 50, 4)
        self.atten_fusion = nn.Sequential(
            nn.Linear(100, 1),
            # nn.Linear(300, 1),
            nn.ReLU(),
            nn.Softmax(dim=0)
        )

        ## All Version
        self.trans_enc = TransformerModel(100, 4, 100, 6)
        self.linear_pre = nn.Sequential(
            nn.Linear(120, 60),
            nn.Dropout(p=0.3),
            nn.ReLU(),
            nn.Linear(60, 30),
        )
        # self.trans_enc = TransformerModel(300, 4, 100, 8)
        # self.linear_pre = nn.Sequential(
        #     nn.Linear(350, 200),
        #     nn.Dropout(p=0.3),
        #     nn.ReLU(),
        #     nn.Linear(200, 100),
        #     nn.Dropout(p=0.1),
        #     nn.ReLU(),
        #     nn.Linear(100, 50),
        # )
        ##############################################
        ## Dot Product Version
        # self.linear_pre = nn.Sequential(
        #     nn.Linear(120, 30),
        # )
        ##############################################
        ## Distribution Version
        # self.linear_pre = nn.Sequential(
        #     nn.Linear(120, 50),
        #     nn.Dropout(p=0.5),
        #     nn.ReLU()
        # )
        # self.mu_linear = nn.Sequential(
        #     nn.Linear(50, 40),
        #     nn.Dropout(p=0.1),
        #     nn.ReLU(),
        #     nn.Linear(40, 30),
        #     nn.ReLU()
        # )
        # self.sigma_linear = nn.Sequential(
        #     nn.Linear(50, 40),
        #     nn.Dropout(p=0.1),
        #     nn.ReLU(),
        #     nn.Linear(40, 30),
        #     nn.ReLU()
        # )
        
    def forward(self, record, record_order, cos_embedding_model, sgrade, cos_pos_idx):
        trans_output = []
        for idx in range(len(record_order)):
            with torch.no_grad():
                cur_input = cos_embedding_model(record_order[idx])
            cur_input *= record[idx][record_order[idx]].view(-1, 1)
            output = self.trans_enc(cur_input.float().unsqueeze_(1), cos_pos_idx[idx]).sum(dim=1)
            output *= self.atten_fusion(output.clone())
            o_sum = output.sum(dim=0)
            trans_output.append(o_sum)

        trans_output = torch.stack(trans_output)
        # sgrade = sgrade.repeat(1, 50)
        sgrade = sgrade.repeat(1, 20)
        trans_output = torch.cat((trans_output, sgrade), dim=1)
        temp = self.linear_pre(trans_output)
        # mu = self.mu_linear(temp)
        # sigma = self.sigma_linear(temp)

        return temp
        # return mu, sigma