import matplotlib.pyplot as plt
import torch
import numpy as np

def handmad_BCELoss(x, y):
    return -(y*np.log(x)+(1-y)*np.log(1-x))

for y_d in np.arange(0., 1.1, 0.1):
    x = np.random.rand(100)
    x.sort()
    print(x)
    y = handmad_BCELoss(x, y_d)
    print(y)
    plt.plot(x,y)
    plt.title(f'y={y_d}')
    plt.show()